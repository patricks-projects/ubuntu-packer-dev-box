terraform {
  backend "gcs" {
    bucket  = "ubuntu-packer-248022-remote-state"
    prefix  = "terraform/packer-state"
	  credentials = "ubuntu-packer.json"
  }
  
  required_providers {
    google = {
      source = "hashicorp/google"
    }
  }
}

provider "google" {
  project = "ubuntu-packer-248022"
  region  = var.region
  zone    = "us-central1-c"
  credentials = file("ubuntu-packer.json")
}

resource "google_compute_instance" "gcp_instance" {
  name = var.instance-name
  machine_type = var.vm_type
  min_cpu_platform = "Intel Haswell"

  zone = var.zone

  tags = [
    "${var.network}-firewall-ssh",
    "${var.network}-firewall-http",
    "${var.network}-firewall-https",
    "${var.network}-firewall-icmp",
    "${var.network}-firewall-openshift-console",
    "${var.network}-firewall-secure-forward",
  ]

  advanced_machine_features {
    enable_nested_virtualization = true
  }

  boot_disk {
    initialize_params {
      image = "ubuntu-2004-focal-v20210720"
    }
  }

  metadata = {
    hostname = var.instance-name
	  sshKeys  = "${var.ssh_username}:${file(var.ssh_username_public_key)}"
  }

  network_interface {
    subnetwork = google_compute_subnetwork.subnet.name
    
    access_config {
      // Ephemeral IP
    }
  }

  scheduling {
    preemptible = "false"
    automatic_restart = "false"
  }
  
  timeouts {
	create = "60m"
  }
}
