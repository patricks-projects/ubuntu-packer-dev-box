variable "region" {
	description = "The region to create the resource in"
	default     = "us-central1"
}

variable "zone" {
	description = "The specific zone to create the resource in"
	default     = "us-central1-a"
}

variable "instance-name" {
	description = "What to name the instance"
	default     = "ubuntu-packer-instance"
}

variable "vm_type" {
	description = "The instance size to use for the disk"
	default     = "custom-2-4096"
}

variable "disk_size" {
	description = "How much hard disk to provision"
	default     = "90"
}

variable "network" {
	description = "the network name"
	default     = "ubuntu-packer-network"
}

variable "ssh_username" {
	description = "the username to add the SSH key to"
	default     = "ubuntu"
}

variable "ssh_username_public_key" {
	description = "the public key to add for the user"
	default     = "id_rsa.pub"

}
