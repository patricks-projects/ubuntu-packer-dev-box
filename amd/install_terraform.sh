# Install unzip (we'll need it)
apt-get update -qq
apt-get install -qq -y unzip
apt-get install -qq -y wget

# Install terraform (we need the specific version)
wget --no-check-certificate https://releases.hashicorp.com/terraform/1.1.7/terraform_1.1.7_linux_amd64.zip
unzip -o ./terraform_1.1.7_linux_amd64.zip
mv -f terraform /usr/local/bin/