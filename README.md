# ubuntu-packer-dev-box

This is a project using Hashicorp Packer. The packer settings will create a Vagrant box file that will contain a common set of development tools that will automatically update each time the packer build is run.

## Where to download the box
This box is automatically published to Vagrant Cloud at the following location: [rice-patrick/ubuntu-bionic-dev-box](https://app.vagrantup.com/rice-patrick/boxes/ubuntu-bionic-dev-box)

## Getting started
Ensure that you have vagrant installed by typing the following command into your command line: `vagrant --version`. While the box doesn't require a specific version of vagrant per se, it is only tested with the most recent version of vagrant.

Once you've ensured vagrant is installed, you can run the box with the following command:
`vagrant init rice-patrick/ubuntu-bionic-dev-box && vagrant up`

## How to Contribute
The project is comprised of essentially 3 pieces:
* `ubuntu-*.json` - The packer configuration. This file defines all the scripts that will be run as part of the build. As a general rule, these will all be stored within the `scripts` folder. These scripts will always be run in the order defined within the JSON file
* `./scripts` and `./arm_scripts` - These 2 folders contain all scripts that will be run on the box. `./scripts` is for x86 vm and `./arm_scripts` for ARM vm. As a general rule, each script should be defined within it's own file
* `./terraform-config` - This defines the infrastructure configuration created on GCP for actually creating the box.

Within `builds` and `http/http-arm` are scripts for doing the base installation of ubuntu. The `templates` folder simply contains the Vagrantfile that gets packaged with the box.
1
